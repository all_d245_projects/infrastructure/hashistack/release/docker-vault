---
# 1password-cli install steps: https://developer.1password.com/docs/cli/get-started/

- name: Determine system architecture
  vars:
    architecture_mapping:
      x86_64: "amd64"
      i386: "386"
      armv7l: "arm"
      aarch64: "arm64"
  set_fact:
    op_arch: "{{ architecture_mapping[ansible_architecture] | default(ansible_architecture) }}"

- name: Download 1Password CLI zip
  get_url:
    url: "https://cache.agilebits.com/dist/1P/op2/pkg/v{{ packer_vars.one_password_cli_version }}/op_linux_{{ op_arch }}_v{{ packer_vars.one_password_cli_version }}.zip"
    dest: "/tmp/op.zip"

- name: Create tmp/op directory
  file:
    path: "tmp/op"
    state: directory

- name: Unzip 1Password CLI
  unarchive:
    remote_src: true
    src: "/tmp/op.zip"
    creates: "/tmp/op/op"
    dest: "/tmp/op"

- name: Move 1Password CLI to /usr/local/bin
  command:
    cmd: "mv /tmp/op/op /usr/local/bin"

- name: Remove temporary files
  file:
    path: "/tmp/op.zip"
    state: absent

- name: Create group onepassword-cli if not exists
  group:
    name: "onepassword-cli"
    state: present

- name: Change group ownership of /usr/local/bin/op
  file:
    path: "/usr/local/bin/op"
    group: "onepassword-cli"

- name: Set the setgid permission on /usr/local/bin/op
  file:
    path: "/usr/local/bin/op"
    mode: "g+s"

- name: Check 1password-cli version
  command: "op --version"
  register: one_password_version
  ignore_errors: true

- name: Fail if 1Password CLI version check fails
  fail:
    msg: "1Password CLI installation failed."
  when: one_password_version.rc != 0

- name: Check 1password-cli connection with service token
  environment:
    OP_SERVICE_ACCOUNT_TOKEN: "{{ packer_vars.one_password_service_account_token }}"
  command: "op user get --me"
  register: one_password_connection_test
  ignore_errors: true

- name: Fail if 1Password CLI connection check fails
  fail:
    msg: "1Password CLI installation failed."
  when: one_password_connection_test.rc != 0

- name: Display 1password CLI connection check result
  debug:
    var: one_password_connection_test.stdout_lines

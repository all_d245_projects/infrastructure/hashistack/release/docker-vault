#!/usr/bin/bash

is_tailscale_authkey_set() {
    [ -n "$TAILSCALE_AUTHKEY" ]
}

# Function to handle cleanup
cleanup() {
    # logout after operation
    if is_tailscale_authkey_set; then
      tailscale logout
    fi
}

# Function to start tailscale daemon and login
start_tailscale() {
  # start tailscaled
  tailscaled --state="mem:" 2>&1 &

  # wait a few seconds before bringing up
  sleep 5

  # bring tailscale up
  tailscale up --authkey=${TAILSCALE_AUTHKEY} --hostname="hashistack-docker-vault"

  # verify tailscale is up
  tailscale ip -4
}

# Set up trap to call cleanup function on script exit or error
trap cleanup ERR EXIT SIGTERM

if is_tailscale_authkey_set; then
    start_tailscale
fi

"${@}"
# Docker-Vault

The docker-vault builder creates a docker image using the packer [docker builder](https://developer.hashicorp.com/packer/integrations/hashicorp/docker/latest/components/builder/docker) to support vault builds. Provisioning is handled by shell scripts and ansible.

**<u>Configurations</u>**  
The docker image uses the official [python](https://hub.docker.com/_/python) image as a base image with the following configurations:

- `ENTRYPOINT`: An entrypoint script to override the default behavior of the base packer image.
- `ENV PACKER_PLUGIN_PATH`: Path to the directory where packer stores downloaded plugins.
- `ENV PACKER_CACHE_DIRE`: Path to the directory where packer stores its cahce.

The packer image is provisioned using the ansible provisioner with the following installing steps:
1. Create required directories.
2. Install 1password cli
3. Install packer
4. Install vault

Configuration variables can be found in the file `packer/docker/variables.pkr.hcl`.

**<u>Building The Image</u>**  
All commands should be executed from the root directory.

1. Install all required dependencies locally:
   - [packer](https://developer.hashicorp.com/packer/install?product_intent=packer)

2. Install the required packer plugins for the docker vault builder:
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer init -only='docker-vault.*' packer/docker-vault
   ```

3. Validate the configurations:
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer validate -only='docker-vault.*' packer/docker-vault
   ```

4. Build the image:
   a. prod (The prod build pushes to the registry): 
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer build -only=docker-vault.docker.prod packer/docker-vault
   ```

   b. local (The local build does not push to the registry): 
   ```bash
   PACKER_CACHE_DIR=files/packer/cache/.packer_cache \
   PACKER_PLUGIN_PATH=files/packer/cache/.packer_plugins \
   packer build -only=docker-vault.docker.local packer/docker-vault
   ```

**<u>Running The Image</u>**  
After the image has been succesfully built, it can be run by passing commands directly to the container eg (using local build): `docker run --rm hashistack/docker-vault:local vault -version`.
The image includes the tailscale daemon. If it's requried to connect to a tailnet, simply pass the tailscale auth-key during the container run along with required parameters:  
```bash
docker run --rm --privileged \
--device /dev/net/tun:/dev/net/tun \
--env TAILSCALE_AUTHKEY={tailscale-authkey} \
hashistack/docker-vault:local vault -version
```

**<u>Pushing The Image To The Registry</u>**  
By default, the build will push the image to the cloud container registry using the default tags during prod builds if the docker push variables have not been overrided.

During local builds, its strongly recommended to modify the following variables:
- `docker_tag_registry=hashistack/docker-vault`
- `docker_tag_tags=["local"]`
